# Agalma Web Appliance

[Agalma](https://bitbucket.org/caseywdunn/agalma) is an automated workflow for
transcriptome assembly and phylogenomic analysis developed by the [Dunn
Lab](http://dunnlab.org) at Brown University.

The Agalma Web Appliance is a pre-configured, easy-to-use image for performing
a transcriptome assembly of paired-end Illumina RNA-Seq data in the cloud.

Use the following URL to launch an instance of the Agalma Web Appliance (Agalma
version 0.4.0, agalma-web-app rev. 7ad4052) in Amazon EC2 (region `us-east-1`)
through your web browser using an existing EC2 account:

[https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#LaunchInstanceWizard:ami=ami-e4c8208c](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#LaunchInstanceWizard:ami=ami-e4c8208c)

We recommend choosing a `c3.8xlarge` instance (32 cores, 60GB memory) and
configuring a security group that only opens TCP port 80 (HTTP) to your local IP
address.

**Advanced**: To launch an instance with the EC2 command-line tools, call
`ec2-run-instances` specifying the AMI `ami-e4c8208c` along with a security
group and key that you have previously configured.

## Video Tutorial

[![screenshot](https://bitbucket.org/caseywdunn/agalma-web-app/raw/master/screenshot.png =640x400)](http://vimeo.com/100893567)

[Getting Started with the Agalma Web Appliance](http://vimeo.com/100893567) on Vimeo.

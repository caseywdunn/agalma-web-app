#!/bin/bash

# This script cleans up your EC2 instance before baking a new AMI.
# Run the following command in a root shell:
#
# bash <(curl -s https://gist.github.com/justindowning/5921369/raw/ami-clean.sh)

function print_green {
  echo -e "\e[32m${1}\e[0m"
}

print_green 'Clean Apt'
sudo apt-get -y autoremove
sudo aptitude clean
sudo aptitude autoclean

print_green 'Remove SSH keys'
[ -f /home/ubuntu/.ssh/authorized_keys ] && shred -uz /home/ubuntu/.ssh/authorized_keys
 
print_green 'Cleanup log files'
find /var/log -type f | while read f; do sudo shred -z $f; done
forever cleanlogs

print_green 'Cleanup test runs'
rm -rf /mnt/*
rm -rf /home/ubuntu/agalma-web-app/public/report*
rm -f /home/ubuntu/agalma-web-app/public/runlog.txt

print_green 'Cleanup bash history'
unset HISTFILE
[ -f /root/.bash_history ] && sudo shred -uz /root/.bash_history
[ -f /home/ubuntu/.viminfo ] && shred -uz /home/ubuntu/.viminfo
[ -f /home/ubuntu/.gitconfig ] && shred -uz /home/ubuntu/.gitconfig
[ -f /home/ubuntu/.bash_history ] && shred -uz /home/ubuntu/.bash_history

print_green 'AMI cleanup complete!'

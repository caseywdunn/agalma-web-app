/**
 * Module dependencies.
 */

var express = require('express');
var http = require('http');
var path = require('path');
var fs = require('fs');
var querystring = require('querystring');

// config
var wrapper = path.join(__dirname, 'wrapper.sh');
var tmpdir = '/mnt/scratch/';
var datadir = '/mnt/data/';
var gitversion = fs.readFileSync(path.join(__dirname, '.gitversion'));


var app = express();

// all environments
app.set('port', process.env.PORT || 80);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
var publicdir = path.join(__dirname, 'public/');
app.use(express.static(publicdir));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// global state
var uploads = {};
var fastq = {1: null, 2: null};
var agalma = null;
var runlog = '';
var args = null;
var error = null;
var finished = false;

server = http.createServer(app);
server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

io = require('socket.io').listen(server);
io.sockets.on('connection', function(socket){
  socket.emit('console', runlog);
  if (error) socket.emit('error', error);
  if (finished) socket.emit('finished', null);
});

function render(req, res) {
  data = {gitversion: gitversion}
  if (agalma) {
    res.render('run', data);
  } else {
    res.render('upload', data);
  }
}

app.get('/', render);

function run_agalma() {
  error = null;
  finished = false;
  agalma = require('child_process').spawn(wrapper, args)
    .on('exit', function(code, signal){
      if (code == 0) {
        finished = true;
        io.sockets.emit('finished', null);
        fs.writeFileSync(publicdir+'runlog.txt', runlog);
      } else {
	error = 'return code: ' + code + ', signal: ' + signal;
        io.sockets.emit('error', error);
      }
    })
    .on('error', function(err) {
      error = err;
      io.sockets.emit('error', error);
    });
  agalma.stdout.on('data', function(data){
    if (Buffer.isBuffer(data)) data = data.toString();
    io.sockets.emit('console', data);
    runlog += data;
  });
  agalma.stderr.on('data', function(data){
    if (Buffer.isBuffer(data)) data = data.toString();
    io.sockets.emit('console', data);
    runlog += data;
  });
}

function stop_agalma() {
  if (agalma) {
    agalma.kill();
    agalma = null;
  }
}

app.post('/', function(req, res) {
  post = req.body;
  console.log(post);
  if ('abort' in post) {
    stop_agalma();
  } else if ('restart' in post) {
    stop_agalma();
    run_agalma();
  } else if (('run' in post) && (!agalma) && fastq[1] && fastq[2]) {
    args = new Array('--paths', datadir+fastq[1], datadir+fastq[2]);
    if (post.id) { args.push('--id'); args.push(post.id); }
    if (post.species) { args.push('--species'); args.push(post.species); }
    if (post.ncbi_id) { args.push('--ncbi_id'); args.push(post.ncbi_id); }
    if (post.itis_id) { args.push('--itis_id'); args.push(post.itis_id); }
    run_agalma();
  } else if (('sra' in post) && (!agalma)) {
    args = new Array('SRA', post.sra);
    run_agalma();
  }
  render(req, res);
});

function json_err(res, code, msg) {
  res.writeHead(200, {'Content-Type': 'application/json'});
  res.end(JSON.stringify({
    jsonrpc: 2.0, code: code, error: msg, result: null, id: 'id'}));
}

function upload(req, res, i) {
  form = new require('formidable').IncomingForm();
  form.uploadDir = tmpdir;
  form.parse(req, function(err, fields, files) {
    if (err) {
      json_err(res, 100, err);
      return;
    }
    var filename = '';
    if ('name' in fields) filename = fields.name;
    else if ('name' in files.file) filename = files.file.name;
    else {
      json_err(res, 101, 'Failed to receive file name.');
      return;
    }
    if (!(filename in uploads)) {
      uploads[filename] = {chunks: {}, count: 0}
    }
    uploads[filename].chunks[fields.chunk] = files.file.path;
    uploads[filename].count++;
    if (uploads[filename].count == fields.chunks) {
      if (fs.existsSync(datadir+filename)) fs.truncateSync(datadir+filename, 0);
      for (var j=0; j<fields.chunks; j++) {
        fs.appendFileSync(
          datadir+filename, fs.readFileSync(uploads[filename].chunks[j]));
        fs.unlinkSync(uploads[filename].chunks[j]);
      }
      if (fs.existsSync(datadir+fastq[i])) {
        delete uploads[fastq[i]];
        fs.unlinkSync(datadir+fastq[i]);
      }
      fastq[i] = filename;
    }
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end(JSON.stringify({jsonrpc: 2.0, error: null, result: null, id: 'id'}));
  });
}

app.post('/upload1', function(req, res) {
  upload(req, res, 1);
});

app.post('/upload2', function(req, res) {
  upload(req, res, 2);
});

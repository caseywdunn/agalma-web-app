#!/bin/bash
sudo apt-get update
sudo apt-get install -y nodejs node npm git libcap2-bin python-dev python-pip libxml2-dev libxslt1-dev python-matplotlib build-essential gcc-4.4 g++-4.4 cmake curl git unzip zip default-jre-headless libncurses5-dev zlib1g-dev lvm2 python-lxml sqlite3
bash -c "$(curl -fsSL https://bitbucket.org/mhowison/bib/raw/master/install.sh)"
export PATH=/opt/bib/active/bin:$PATH
echo "export PATH=/opt/bib/active/bin:\$PATH" >>~/.bash_profile
yes | sudo cpan -i URI::Escape FindBin
bib install -f biolite-tools/0.4.0 blast/2.2.29+ bowtie/1.0.0 bowtie2/2.1.0 fastqc/0.10.1 gblocks/0.91b mafft/7.130 mcl/12-135 parallel/20130922 raxml/7.7.6 rsem/1.2.9 samtools/0.1.19 sratoolkit/0.2.3.4-2 transdecoder/r2012-08-15 trinity/r20140413p1
sudo pip install agalma
sudo npm install forever -g
sudo sed -i -e '1 s/node/nodejs/' /usr/local/lib/node_modules/forever/bin/forever
wget http://eutils.ncbi.nlm.nih.gov/eutils/dtd/20060628/esearch.dtd
sudo mv esearch.dtd /usr/local/lib/python2.7/dist-packages/Bio/Entrez/DTDs/
sudo setcap 'cap_net_bind_service=+ep' /usr/bin/nodejs
git clone https://bitbucket.org/caseywdunn/agalma-web-app.git
cd agalma-web-app
git rev-parse --short HEAD >.gitversion
npm install
echo "#!/bin/bash
umount /mnt
lvremove raid0
vgremove vg0
DEVICES=(\$(ls /dev/xvd[!a]))
echo Creating RAID for ephemeral volumes: \${DEVICES[@]}
for d in \${DEVICES[@]}; do pvcreate \$d; done
pvscan
vgcreate vg0 \${DEVICES[@]}
vgscan
lvcreate --stripes \${#DEVICES[@]} --stripesize 64 --extents 100%FREE --name raid0 vg0
lvscan
mkfs.ext4 /dev/vg0/raid0
mount /dev/vg0/raid0 /mnt
mkdir -p /mnt/data
mkdir -p /mnt/scratch
chown ubuntu:ubuntu -R /mnt
sudo -u ubuntu forever start /home/ubuntu/agalma-web-app/app.js
exit 0" >rc.local
sudo mv rc.local /etc/rc.local
sudo chmod 755 /etc/rc.local
sudo chown root:root /etc/rc.local
sudo /etc/rc.local

#!/bin/bash

set -e

export PATH=/opt/bib/active/bin:$PATH
export HTML=$HOME/agalma-web-app/public
export PYTHONUNBUFFERED=1

CPU=$(grep -c '^processor' /proc/cpuinfo)
MEM=$(free -m | awk '/^Mem/{print $4;}')

export BIOLITE_RESOURCES="database=/mnt/biolite.sqlite,outdir=/mnt/analyses,threads=${CPU},memory=${MEM}M"

if [ "$1" == "SRA" ]; then
  cd /mnt/data
  bl-sra import -e agalma-web-app $2
  bl-catalog all >/mnt/catalog.txt
else
  bl-catalog insert "$@" | tee /mnt/catalog.txt
fi

ID=$(head -1 /mnt/catalog.txt | cut -d' ' -f1)

cd /mnt/scratch

agalma transcriptome --id $ID

agalma report --id $ID --outdir $HTML/report
agalma resources --id $ID --outdir $HTML/report

cd $HTML
zip -r report.zip report
